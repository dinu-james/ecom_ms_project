package com.cts.personservice.service;


import com.cts.personservice.entity.AddressEntity;
import com.cts.personservice.entity.PersonEntity;
import com.cts.personservice.entity.PersonSearchEntity;
import com.cts.personservice.exception.RecordNotFoundException;
import com.cts.personservice.model.*;
import com.cts.personservice.repository.PersonRepository;
import com.cts.personservice.service.converter.PersonConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.cts.personservice.util.EncodingUtils.decodeVersion;
import static com.cts.personservice.util.EncodingUtils.encodeVersion;

@Service
public class AddressService {

    private static final Logger LOG = LoggerFactory.getLogger(AddressService.class);

    private final PersonConverter personConverter;
    private final PersonRepository personRepository;

    public AddressService(PersonConverter personConverter, PersonRepository personRepository) {
        this.personConverter = personConverter;
        this.personRepository = personRepository;
    }
// add address
    public String addAddress(String personId, String ifMatch, Address address) {
        LOG.info("Updating address with addressId={}, version={}", personId, decodeVersion(ifMatch));
        Optional<PersonEntity> personEntity = personRepository.findById(personId);
        personEntity.orElseThrow(() -> new RecordNotFoundException("Person not found with personId=" + personId));
        AddressEntity addressEntity = personConverter.addressModelToEntity(address);
        if(personEntity.get().getAddress()==null){
            personEntity.get().setAddress(new ArrayList<>());
        }
        personEntity.get().getAddress().add(addressEntity);
        personRepository.save(personEntity.get());
        return encodeVersion(personEntity.get().getVersion());

    }

    public void deleteAddress(String personId, String addressId) {
        LOG.info("deleting address addressId={} for personId={}", addressId,personId);
        Optional<PersonEntity> personEntity =personRepository.findById(personId);
        personEntity.orElseThrow(() -> new RecordNotFoundException("Person not found with personId=" + personId));
        if(!Objects.isNull(personEntity.get().getAddress())){
            List<AddressEntity> addressEntity = personEntity.get().getAddress();
            if(!addressEntity.removeIf(item->item.getAddressId().equals(addressId))){
                throw(new RecordNotFoundException("Address not found with AddressId=" + addressId));
            }
            personEntity.get().setAddress(addressEntity);
        }
        personRepository.save(personEntity.get());
    }
}
