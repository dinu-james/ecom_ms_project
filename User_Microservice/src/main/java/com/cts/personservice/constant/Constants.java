package com.cts.personservice.constant;

public final class Constants {
    private Constants() {
    }

    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final String APP_NAME = "/person-service";

    // Request Header related Constants
    public static final class RequestHeader {
        private RequestHeader() {
        }

        public static final String X_TENANT_ID = "X-Tenant-Id";

    }
    // Hystrix related Constants
    public static final class Hystrix {
        private Hystrix() {
        }

        public static final String X_TENANT_ID = "X-Tenant-Id";

    }

    // Collection related Constants
    public static final class Collections {
        private Collections() {
        }

        public static final String PERSON_COLLECTION = "persons";

    }
}
